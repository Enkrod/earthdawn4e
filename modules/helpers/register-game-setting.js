import earthdawn4eItemSheet from '../sheets/earthdawn-4e-item-sheet.js';

export function registerGameSetting() {
  game.settings.register('earthdawn4e', 'stepTableEdition', {
    name: 'Step Table',
    hint: 'Which Step Table Do You Want to Use?',
    scope: 'world',
    type: String,
    default: 'step4',
    config: true,
    choices: {
      step4: game.i18n.localize('earthdawn.e.editionFourth'),
      step3: game.i18n.localize('earthdawn.e.editionThird'),
      step1: game.i18n.localize('earthdawn.e.editionfirst'),
      stepC: game.i18n.localize('earthdawn.e.editionClassic'),
    },
  });

  game.settings.register('earthdawn4e', 'theme', {
    name: 'Theme',
    hint: 'Which theme do you want to use?',
    scope: 'world',
    type: String,
    default: 'earthdawn4e',
    config: true,
    choices: {
      earthdawn4e: 'earthdawn4e',
      taka: 'Takas Theme',
    },
  });

  game.settings.register('earthdawn4e', 'sortTalents', {
    name: 'Talent Sorting',
    hint: 'How would you like your talents to be sorted?',
    scope: 'world',
    type: String,
    default: 'default',
    config: true,
    choices: {
      default: 'Default',
      split: 'Split',
    },
  });

  game.settings.register('earthdawn4e', 'sortSpells', {
    name: 'Spell Sorting',
    hint: 'How would you like your spells to be sorted?',
    scope: 'world',
    type: String,
    default: 'default',
    config: true,
    choices: {
      default: 'Default',
      split: 'Split',
    },
  });

  game.settings.register('earthdawn4e', 'trackAmmo', {
    name: 'Track Ammo',
    hint: 'Check Box to Automatically Track and Enforce Ammo',
    scope: 'world',
    type: Boolean,
    default: false,
    config: true,
    choices: {
      true: 'Yes',
      false: 'No',
    },
  });

  //game setting for broken-link aktivation
  game.settings.register('earthdawn4e', 'brokenLinks', {
    name: 'Broken Links',
    Hint: 'Do You Want to Show Links to Unowned Products?',
    //gibt es auch scope system???
    scope: 'world',
    type: String,
    default: 'default',
    config: true,
    choices: {
      default: 'Show',
      hide: 'Hide',
    },
  });

  game.settings.register('earthdawn4e', 'moneyConversionRate', {
    name: 'Money Conversion Rate',
    hint: 'The conversion rate from Copper to Silver to Gold',
    scope: 'world',
    requiresReload: false,
    type: Number,
    default: 10,
  });

  //  LP SPENDING AND TRACKING //

  game.settings.register('earthdawn4e', 'lpSpendingOnClick', {
    name: 'LP Spending - always on click',
    hint: 'When activated, simple clicks on upgrade buttons will open the LP spending dialog (no shift + click necessary).',
    scope: 'client',
    type: Boolean,
    default: false,
    config: true,
  });

  game.settings.register('earthdawn4e', 'avgSilverSkillTrainWeek', {
    name: 'LP Spending - average cost per week skill training',
    hint: 'How much silver does an average week of skill training cost?',
    scope: 'world',
    type: Number,
    default: 10,
    config: true,
  });
}
