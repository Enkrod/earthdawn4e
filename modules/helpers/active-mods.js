//###########################################################
/**Active Mods */
/*  active Mods will be all modifiers affecting rolls.*/
/* 
List of finished Active Mods
- Base steps or Attributes
- Ranks
- Wound modifier
- Bonus to all rolls modifier
- Bonus to all close Combat Attacks
- Situation Modifier - Knockdown
- Situation Modifier - Harried
- Combat Modifier - Defensive Stance
- Combat Modifier - Aggressive Stance
*/
/*
List of open Active Mods
- Situation Modifier - Blindsided
- Situation Modifier - Cover
- Situation Modifier - Darkness
- Situation Modifier - Dazzled
- Situation Modifier - Overwhelmed
- Situation Modifier - Impaired Movement
- Situation Modifier - Range
- Situation Modifier - Surprised
- Combat Modifier - Attacking to Knockdown
- #311 - Combat Modifier - Attacking to stun 
- Combat Modifier - called shot
- Combat Modifier - Jump Up --> function is included. Active mods + chat are missing
- Combat Modifier - Setting against a charge
- Combat Modifier - Shattering a shield
- Combat Modifier - splitting Movement
- Combat Modifier - Tail attack
- Low Strength / low dex Modifier
- Bonus to all damage of close Attacks
- Bonus to all ranged attacks
- Bonus to all damage of ranged attacks
- Bonus to all damage
- Bonus to all attacks
- etc.
*/
//###########################################################

import { capitalize } from '../helpers/capitalize.js';
import { ED4E } from './config.js';

export async function readMods(actor, inputs, activeMods = []) {
  //#######################################################################################
  /** Base Steps or Attributes */
  //#######################################################################################
  if (inputs.steps) {
    activeMods.push(`${game.i18n.localize('earthdawn.b.baseStep')}: ${inputs.steps}`);
  } else {
    activeMods.push(`${game.i18n.localize('earthdawn.a.attribute')} ${game.i18n.localize('earthdawn.s.step')}: ${inputs.basestep}`);
  }

  //#######################################################################################
  /** Step Modifier from OptionBox */
  //#######################################################################################
  if (inputs.stepModifier) {
    activeMods.push(`${game.i18n.localize('earthdawn.m.modifierStep')}: ${inputs.stepModifier}`);
  }

  //#######################################################################################
  /** Ranks */
  //#######################################################################################
  if (inputs.ranks > 0) {
    let typeName = inputs.type;
    //possible solution see reference https://github.com/foundryvtt/dnd5e/blob/master/module/config.mjs#L21-L30
    // started in config.js.
    
    activeMods.push(`${game.i18n.localize(ED4E.typeTranslations[typeName])} ${game.i18n.localize('earthdawn.r.ranks')}: +${inputs.ranks}`);
  }

  //#######################################################################################
  /** Wound modifier */
  //#######################################################################################
  if (actor.system.wounds > 0) {
    activeMods.push(`${game.i18n.localize('earthdawn.w.wounds')}: -${actor.system.wounds}`);
  }

  //#######################################################################################
  /** Bonus to all rolls modifier */
  //#######################################################################################
  if (actor.system.bonuses.allRollsStep > 0) {
    activeMods.push(`${game.i18n.localize('earthdawn.a.activeEffectAllRolls')}: +${actor.system.bonuses.allRollsStep}`);
  }

  //#######################################################################################
  // Situation Modifier
  //#######################################################################################
  /** Knockdown */
  if (actor.system.tactics.knockeddown === true && inputs.rolltype !== 'jumpUp') {
    activeMods.push(`${game.i18n.localize('earthdawn.c.combatModifierKnockedDown')}: -3`);
  } else {
    /** Defensive Stance && Aggressive Stance */
    if (actor.system.tactics.defensive && inputs.rolltype !== 'knockdown') {
      activeMods.push(`${game.i18n.localize('earthdawn.c.combatOptionsDefensive')}: -3`);
    } else if (actor.system.tactics.aggressive === true && inputs.rolltype === 'attack' && inputs.attackType === 'closeAttack') {
      activeMods.push(`${game.i18n.localize('earthdawn.c.combatOptionsAggressive')}: +3`);
    }
  }
  /** Harried */
  if (actor.system.tactics.harried === true) {
    activeMods.push(`${game.i18n.localize('earthdawn.c.combatModifierHarried')}: -2`); 
  }

  //#######################################################################################
  // attacks
  //#######################################################################################
  /** attack type */
  if (inputs.rolltype === 'attack') {
    if (inputs.attackType === 'closeAttack') {
      /** attack is close Combat */
      activeMods.push(`${game.i18n.localize('earthdawn.a.attackTypeCloseCombat')}`);
      /** Bonus to all close Combat Attacks */
      if (actor.system.bonuses.closeAttack !== 0) {
        activeMods.push(`${game.i18n.localize('earthdawn.a.attackTypeRangeCombat')}: +${actor.system.bonuses.closeAttack}`);
      }
      /** attack is ranged combat */
    } else if (inputs.attackType === 'rangedAttack') {
      activeMods.push(`${game.i18n.localize('earthdawn.a.attackTypeRangeCombat')}`); 
    }
  }

  //console.log(activeMods);
  return activeMods;
}
