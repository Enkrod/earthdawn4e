export function devotionAllowed(inputs) {
  let devotion = 0;
  if (inputs.type === 'devotion' && inputs.devotionrequested === 'true') {
    devotion = 1;
  }
  return devotion;
}
