export function get3eDice(step) {
  const stepsTable = [
    { Step: '1', Dice: 'd6 − 3' },
    { Step: '2', Dice: 'd6 − 2' },
    { Step: '3', Dice: 'd6 − 1' },
    { Step: '4', Dice: 'd6' },
    { Step: '5', Dice: 'd8' },
    { Step: '6', Dice: 'd10' },
    { Step: '7', Dice: 'd12' },
    { Step: '8', Dice: '2d6' },
    { Step: '9', Dice: 'd8 + d6' },
    { Step: '10', Dice: '2d8' },
    { Step: '11', Dice: 'd10 + d8' },
    { Step: '12', Dice: '2d10' },
    { Step: '13', Dice: 'd12 + d10' },
    { Step: '14', Dice: '2d12' },
    { Step: '15', Dice: 'd12 + 2d6' },
    { Step: '16', Dice: 'd12 + d8 + d6' },
    { Step: '17', Dice: 'd12 + 2d8' },
    { Step: '18', Dice: 'd12 + d10 + d8' },
    { Step: '19', Dice: 'd12 + 2d10' },
    { Step: '20', Dice: '2d12 + d10' },
    { Step: '21', Dice: '3d12' },
    { Step: '22', Dice: '2d12 + 2d6' },
    { Step: '23', Dice: '2d12 + d8 + d6' },
    { Step: '24', Dice: '2d12 + 2d8' },
    { Step: '25', Dice: '2d12 + d10 + d8' },
    { Step: '26', Dice: '2d12 + 2d10' },
    { Step: '27', Dice: '3d12 + d10' },
    { Step: '28', Dice: '4d12' },
    { Step: '29', Dice: '3d12 + 2d6' },
    { Step: '30', Dice: '3d12 + d8 + d6' },
    { Step: '31', Dice: '3d12 + 2d8' },
    { Step: '32', Dice: '3d12 + d10 + d8' },
    { Step: '33', Dice: '3d12 + 2d10' },
    { Step: '34', Dice: '4d12 + d10' },
    { Step: '35', Dice: '5d12' },
    { Step: '36', Dice: '4d12 + 2d6' },
    { Step: '37', Dice: '4d12 + d8 + d6' },
    { Step: '38', Dice: '4d12 + 2d8' },
    { Step: '39', Dice: '4d12 + d10 + d8' },
    { Step: '40', Dice: '4d12 + 2d10' },
    { Step: '41', Dice: '5d12 + d10' },
    { Step: '42', Dice: '6d12' },
    { Step: '43', Dice: '5d12 + 2d6' },
    { Step: '44', Dice: '5d12 + d8 + d6' },
    { Step: '45', Dice: '5d12 + 2d8' },
    { Step: '46', Dice: '5d12 + d10 + d8' },
    { Step: '47', Dice: '5d12 + 2d10' },
    { Step: '48', Dice: '6d12 + d10' },
    { Step: '49', Dice: '7d12' },
    { Step: '50', Dice: '6d12 + 2d6' },
    { Step: '51', Dice: '6d12 + d8 + d6' },
    { Step: '52', Dice: '6d12 + 2d8' },
    { Step: '53', Dice: '6d12 + d10 + d8' },
    { Step: '54', Dice: '6d12 + 2d10' },
    { Step: '55', Dice: '7d12 + d10' },
    { Step: '56', Dice: '8d12' },
    { Step: '57', Dice: '7d12 + 2d6' },
    { Step: '58', Dice: '7d12 + d8 + d6' },
    { Step: '59', Dice: '7d12 + 2d8' },
    { Step: '60', Dice: '7d12 + d10 + d8' },
    { Step: '61', Dice: '7d12 + 2d10' },
    { Step: '62', Dice: '8d12 + d10' },
    { Step: '63', Dice: '9d12' },
    { Step: '64', Dice: '8d12 + 2d6' },
    { Step: '65', Dice: '8d12 + d8 + d6' },
    { Step: '66', Dice: '8d12 + 2d8' },
    { Step: '67', Dice: '8d12 + d10 + d8' },
    { Step: '68', Dice: '8d12 + 2d10' },
    { Step: '69', Dice: '9d12 + d10' },
    { Step: '70', Dice: '10d12' },
    { Step: '71', Dice: '9d12 + 2d6' },
    { Step: '72', Dice: '9d12 + d8 + d6' },
    { Step: '73', Dice: '9d12 + 2d8' },
    { Step: '74', Dice: '9d12 + d10 + d8' },
    { Step: '75', Dice: '9d12 + 2d10' },
    { Step: '76', Dice: '10d12 + d10' },
    { Step: '77', Dice: '11d12' },
    { Step: '78', Dice: '10d12 + 2d6' },
    { Step: '79', Dice: '10d12 + d8 + d6' },
    { Step: '80', Dice: '10d12 + 2d8' },
    { Step: '81', Dice: '10d12 + d10 + d8' },
    { Step: '82', Dice: '10d12 + 2d10' },
    { Step: '83', Dice: '11d12 + d10' },
    { Step: '84', Dice: '12d12' },
    { Step: '85', Dice: '11d12 + 2d6' },
    { Step: '86', Dice: '11d12 + d8 + d6' },
    { Step: '87', Dice: '11d12 + 2d8' },
    { Step: '88', Dice: '11d12 + d10 + d8' },
    { Step: '89', Dice: '11d12 + 2d10' },
    { Step: '90', Dice: '12d12 + d10' },
    { Step: '91', Dice: '13d12' },
    { Step: '92', Dice: '12d12 + 2d6' },
    { Step: '93', Dice: '12d12 + d8 + d6' },
    { Step: '94', Dice: '12d12 + 2d8' },
    { Step: '95', Dice: '12d12 + d10 + d8' },
    { Step: '96', Dice: '12d12 + 2d10' },
    { Step: '97', Dice: '13d12 + d10' },
    { Step: '98', Dice: '14d12' },
    { Step: '99', Dice: '13d12 + 2d6' },
    { Step: '100', Dice: '13d12 + d8 + d6' },
  ];
  if (step > 100 || step < 1) {
    ui.notifications.error('This Step Table does Not Support That Number');
    return false;
  }
  return stepsTable[step - 1].Dice;
}
