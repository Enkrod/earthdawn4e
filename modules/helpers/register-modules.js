function getModuleDefault() {
  return {
    loaded: false,
    asked: false,
    systemVersion: '0.1.0',
    moduleVersion: '1.0',
  };
}

function initDefault(packs) {
  const rv = {};
  for (const p of packs) {
    rv[p] = getModuleDefault();
  }
  return rv;
}

export function registerPacks() {
  const packs = [
    'earthdawn-gm-compendium',
    'earthdawn-pg-compendium',
    'earthdawn-companion',
    'legends-shared',
    'legend-toys',
    'earthdawn-questors',
    //german modules
    'earthdawnspielerhandbuch',
    'earthdawnspielleiterhandbuch',
    'earthdawnkompendium',
    'earthdawnquestoren',
    'earthdawnkaers',
    'earthdawnnebelueberdemblutwald',
    'earthdawnmystischepfade',
    'earthdawnkaertardim',
    'earthdawnlegendenvonbarsaive1',
    'earthdawnelfennationen',
    'earthdawnardanyansrache',
    'earthdawntanzderschlange',
  ];

  game.settings.register('earthdawn4e', 'packsInitialized', {
    scope: 'world',
    type: Object,
    default: initDefault(packs),
    config: false,
  });

  const packcfg = game.settings.get('earthdawn4e', 'packsInitialized');

  let updateNeeded = false;

  // Fill in missing packs
  for (const p of packs) {
    if (packcfg[p] === undefined) {
      packcfg[p] = getModuleDefault();
      updateNeeded = true;
    }
  }

  if (updateNeeded) game.settings.set('earthdawn4e', 'packsInitialized', packcfg);
}
