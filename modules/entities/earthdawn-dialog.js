export default class EarthdawnDialog extends Dialog {
  activateListeners(html) {
    super.activateListeners(html);

    html.find('.att-change-button').click((ev) => {
      this._attributeChange(ev);
    });
  }

  _attributeChange(ev) {
    let pointCost = [-2, -1, 0, 1, 2, 3, 5, 7, 9, 12, 15];
    let attribute = $(ev.currentTarget).attr('data-att');
    let direction = $(ev.currentTarget).attr('data-direction');
    let baseValue = Number(document.getElementsByName(attribute)[0].value);
    let attributeAdded = `system.${attribute}added`;
    let test = document.getElementsByName(attributeAdded);
    let attributeAddedCurrent = Number(document.getElementsByName(attributeAdded)[0].value);
    let attributeAddedNew = attributeAddedCurrent;

    if ((attributeAddedCurrent > 7 && direction === 'plus') || (attributeAddedCurrent < -1 && direction === 'minus')) {
      ui.notifications.error(game.i18n.localize('earthdawn.c.charGen2LowHigh'));
      return false;
    } else {
      if (direction === 'plus') {
        attributeAddedNew = attributeAddedCurrent + 1;
      } else if (direction === 'minus') {
        attributeAddedNew = attributeAddedCurrent - 1;
      }
      let newValue = attributeAddedNew + baseValue;
      document.getElementsByName(attributeAdded)[0].value = attributeAddedNew;
      let attributeTotal = `system.${attribute}total`;
      document.getElementsByName(attributeTotal)[0].value = newValue;
      let newPointsSpent = Number(document.getElementsByName(attributeAdded)[0].value) + 2;
      let totalPoints = pointCost[newPointsSpent];
      let attributeSpent = `system.${attribute}points`;
      document.getElementsByName(attributeSpent)[0].value = totalPoints;
      let totalSpent =
        Number(document.getElementsByName('system.dexteritypoints')[0].value) +
        Number(document.getElementsByName('system.strengthpoints')[0].value) +
        Number(document.getElementsByName('system.toughnesspoints')[0].value) +
        Number(document.getElementsByName('system.perceptionpoints')[0].value) +
        Number(document.getElementsByName('system.willpowerpoints')[0].value) +
        Number(document.getElementsByName('system.charismapoints')[0].value);
      document.getElementsByName('system.totalremaining')[0].value = 25 - Number(totalSpent);
    }
  }
}
