import Earthdawn4eActorSheet from './earthdawn-4e-actor-sheet.js';

export default class earthdawn4eNPCSheet extends Earthdawn4eActorSheet {
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      width: 520,
      height: 620,
      classes: ['earthdawn', 'sheet', 'actor', 'creature'],
      tabs: [
        {
          navSelector: '.sheet-tabs',
          contentSelector: '.sheet-body',
          initial: 'main',
        },
      ],
    });
  }

  get template() {
    return `systems/earthdawn4e/templates/actors/sheets/npc-sheet.hbs`;
  }

   //HTML enrich
   async _enableHTMLEnrichment() {
    let enrichment = {};
    enrichment["system.description"] = await TextEditor.enrichHTML(this.actor.system.description, {async: true, secrets: this.actor.isOwner});
    enrichment["system.descriptionGameInfo"] = await TextEditor.enrichHTML(this.actor.system.descriptionGameInfo, {async: true, secrets: this.actor.isOwner});
    return expandObject(enrichment);
  }

  async getData() {
    const sheetData = super.getData();
    sheetData.attacks = sheetData.items.filter(function (item) {
      return item.type === 'attack';
    });

    sheetData.enrichment =  await this._enableHTMLEnrichment();

    console.log('[EARTHDAWN] NPC Data', sheetData);

    return sheetData;
  }

  activateListeners(html) {
    super.activateListeners(html);
    this.baseListeners(html);

    html.find('.step-roll').click((ev) => {
      const att = $(ev.currentTarget).attr('data-att');
      const name = $(ev.currentTarget).attr('data-name');
      let inputs = { attribute: att, name: name };
      this.actor.rollPrep(inputs);
    });

    html.find('.effectTest').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
    const weapon = this.actor.items.get(li.data('itemId'));
    this.actor.NPCDamage(this, weapon.system.damagestep, 0, weapon.system.powerType);
    });

    html.find('.ability-roll').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
    const item = this.actor.items.get(li.data('itemId'));
    const att = item.system.attribute;
    const ranks = item.system.ranks;
    const strain = item.system.strain;
    const type = item.system.type;
    const karmaAllowed = item.system.karma;
    
      const parameters = {
        talent: item.name,
        attribute: att,
        ranks: ranks,
        strain: strain,
        type: type,
        karmaallowed: karmaAllowed,
        karmarequested: this.actor.system.usekarma,
      };
      console.log(parameters)
      this.actor.attributetest(parameters);
    });
  }
}
