import earthdawn4eItemSheet from '../earthdawn-4e-item-sheet.js';

export class TakaItemSheet extends earthdawn4eItemSheet {
  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ['taka-theme'],
      width: 600,
      height: 500,
      tabs: [
        {
          navSelector: '.tabs',
          contentSelector: '.sheet-body',
          initial: 'description',
        },
      ],
    });
  }

  /** @override */
  get template() {
    return `systems/earthdawn4e/templates/taka-theme/items/${this.item.type}-sheet.hbs`;
  }

  /* -------------------------------------------- */
}
