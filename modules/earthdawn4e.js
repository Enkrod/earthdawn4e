import earthdawn4ePCSheet from './sheets/earthdawn-4e-PC-sheet.js';
import earthdawn4eActor from './entities/earthdawn-4e-Actor.js';
import earthdawn4eItemSheet from './sheets/earthdawn-4e-item-sheet.js';
import earthdawn4eNPCSheet from './sheets/earthdawn-4e-NPC-sheet.js';
import earthdawn4eItem from './entities/earthdawn-4e-Item.js';
import { chatListeners } from './helpers/chat-listeners.js';
import { getDice } from './helpers/get-dice.js';
import { rollDiceChat } from './helpers/roll-dice-chat.js';
import earthdawn4eCombat from './entities/earthdawn-4e-combat.js';
import { preloadHandlebarTemplates } from './helpers/preload-handlebar-templates.js';
import { TakaItemSheet } from './sheets/taka-theme/taka-item-sheet.js';
import { registerHandlebarHelpers } from './helpers/handlebar-helpers.js';
import { registerGameSetting } from './helpers/register-game-setting.js';
import { registerPacks } from './helpers/register-modules.js';
import { TakaCharacterSheet } from './sheets/taka-theme/taka-character-sheet.js';
import { TakaCreatureSheet } from './sheets/taka-theme/taka-creature-sheet.js';
import { resourceBox } from './helpers/resource-box.js';
import { spliceUuid } from './helpers/misc-helper.js';

let socket;
Hooks.once('init', function () {
  console.log('Initializing Earthdawn 4E for 0.8.6');

  // CONFIG.debug.hooks = true;
  CONFIG.Actor.documentClass = earthdawn4eActor;
  CONFIG.Item.documentClass = earthdawn4eItem;
  CONFIG.Combat.documentClass = earthdawn4eCombat;
  CONFIG.getDice = getDice;

  // config Request Roll
  CONFIG.TextEditor.enrichers.push({
    pattern: /(@RequestRoll)\[(.*?)\](?:{([^}]+)})?/gi,
    enricher: (match, options) => {
      let returnRoll = document.createElement('a');

      returnRoll.innerHTML = match[3] ?? match[2];
      returnRoll.dataset.talent = match[2];
      returnRoll.dataset.skill = match[2];
      returnRoll.title = 'click to roll';
      returnRoll.classList.add('myRequest');
      return returnRoll;
    },
  });

  //config self roll Text
  //
  CONFIG.TextEditor.enrichers.push({
    pattern: /(@Roll)\[(.*?)\](?:{([^}]+)})?/gi,
    enricher: (match, options) => {
      let returnRoll = document.createElement('span');

      returnRoll.innerHTML = match[3] ?? match[2];
      returnRoll.dataset.talent = match[2];
      returnRoll.dataset.skill = match[2];
      returnRoll.dataset.step = match[2];
      returnRoll.title = 'click to roll';
      returnRoll.classList.add('myRoll');
      return returnRoll;
    },
  });

  registerGameSetting();

  preloadHandlebarTemplates();

  registerHandlebarHelpers();

  //game.initialize = initializePack;

  //listener click to request roll
  $('body').on('click', '.myRequest', async (event) => {
    let chatData = {
      content: `@Roll[${event.target.dataset.talent}]`,
    };

    await ChatMessage.create(chatData, {});
  });

  //listener click to self roll
  $('body').on('click', '.myRoll', async (event) => {
    let actor = game.user.character;
    let talent = actor.items.filter((item) => item.name === event.target.dataset.talent);
    let inputs = { talentID: talent[0].id, rolltype: '' };
    actor.rollPrep(inputs);
  });

  const brokenLinks = game.settings.settings.get('earthdawn4e.brokenLinks').choices[game.settings.get('earthdawn4e', 'brokenLinks')];
  console.log('[EARTHDAWN] Selected broken links', brokenLinks);
  if (brokenLinks === 'Hide broken links') {
    $('body').addClass('brokenLinks');
  }

  const theme = game.settings.settings.get('earthdawn4e.theme').choices[game.settings.get('earthdawn4e', 'theme')];
  console.log('[EARTHDAWN] Selected Theme', theme);

  Actors.unregisterSheet('core', ActorSheet);
  Actors.registerSheet('earthdawn4e', earthdawn4ePCSheet, {
    makeDefault: theme === 'earthdawn4e',
    types: ['pc', 'npc'],
  });
  Actors.registerSheet('earthdawn4e', earthdawn4eNPCSheet, {
    makeDefault: theme === 'earthdawn4e',
    types: ['creature'],
  });
  Actors.registerSheet('Takas Theme', TakaCharacterSheet, {
    makeDefault: theme === 'Takas Theme',
    types: ['pc', 'npc'],
  });
  Actors.registerSheet('Takas Theme', TakaCreatureSheet, {
    makeDefault: theme === 'Takas Theme',
    types: ['creature'],
  });

  Items.unregisterSheet('core', ItemSheet);
  Items.registerSheet('earthdawn4e', earthdawn4eItemSheet, {
    makeDefault: theme === 'earthdawn4e',
  });
  Items.registerSheet('Takas Theme', TakaItemSheet, {
    makeDefault: theme === 'Takas Theme',
  });

  Hooks.on('renderChatLog', (log, html) => chatListeners(html));

  Hooks.once('ready', () => {
    // Listen for dice icon click
    const diceIconSelector = '#chat-controls i.fas.fa-dice-d20';
    $(document).on('click', diceIconSelector, () => {
      rollDiceChat();
    });
    socket = socketlib.registerSystem('earthdawn4e');
    socket.register('damage', assignDamage);
    socket.register('effect', assignSpellEffect);
    registerPacks();
    importAdventures();
  });

  Hooks.on('preCreateItem', (item) => {
    if (!item.parent) return;

    if (item.type === 'spell') {
      //   console.log(item.effects)
      //   ui.notifications.info("Item is a Spell")
      //  item.effects.forEach((effect) =>{
      //   console.log(effect)
      //   console.log(effect.transfer)
      //   effect.update({"transfer": false})
      //   console.log(effect.transfer);
      //  })
    }

    const actor = item.parent;
    const namegivers = actor.items.filter(function (item) {
      return item.type === 'namegiver';
    });

    if (namegivers.length > 0 && item.type === 'namegiver') {
      ui.notifications.error(game.i18n.localize('earthdawn.n.namegiverAlready'));
      return false;
    }
  });

  Hooks.on('renderSidebarTab', (app, html) => {
    if (app instanceof Settings) {
      const button1Text = game.i18n.localize('earthdawn.l.linkMessage');
      const button2Text = game.i18n.localize('earthdawn.l.linkMessage2');
      let button1 = $(`<button><i class="fas fa-hand-holding-heart"></i> ${button1Text}</button>`);
      let button2 = $(`<button><i class="fas fa-book"></i> ${button2Text}</button>`);
      html.find('#settings-documentation').append(button1);
      html.find('#settings-documentation').append(button2);
      button1.click(() => {
        window.open('https://gitlab.com/fattom23/earthdawn4e/-/wikis/Home');
      });
      button2.click(async () => {
        let resourceInputs = {};
        resourceInputs.text = `<a class="link-item" href="https://fasagames.com" target="_blank">FASA Website</a><br/>
        <a class="link-item" href="http://pandagaminggrove.blogspot.com/" target="_blank">Panda Gaming Grove: Alternate Rules and Items from Morgan Weeks</a><br/>
        <a class="link-item" href="https://championschallenge.fasagames.com/comic/champions-challenge-1/" target="_blank">Champion's Challenge Webcomic</a><br/>
        <a class="link-item" href="https://anchor.fm/edsg-podcast" target="_blank">Earthdawn Survival Guide</a><br/>
        <a class="link-item" href="https://www.audible.com/pd/Namegivers-An-Earthdawn-Actual-Play-Podcast-Podcast/B08K58JNJX" target="_blank">Namegivers: An Earthdawn Actual Play Podcast</a><br/>
        <a class="link-item" href="https://www.youtube.com/channel/UCD7Ohx0WZkHuJS6UelxEM_w" target="_blank">Mystic Path Actual Play</a><br/>
        <a class="link-item" href="http://legendsofearthdawn.com/" target="_blank">Legends of Earthdawn Actual Play Podcast</a><br/>
        <a class="link-item" href="https://www.youtube.com/watch?v=z9O6kKGa4eM&t=1s" target="_blank">Relative Dimension Actual Play Videos</a><br/>
        <a class="link-item" href="https://www.youtube.com/channel/UCP3tmoj3OC6v3OGRkf7vQpw" target="_blank">Far Scholar Earthdawn</a><br/>
        `;
        await resourceBox(resourceInputs);
      });
    }
  });

  Hooks.on('createItem', async (item) => {
    if (item.type === 'namegiver') {
      const actor = item.parent;
      // actor.update({ 'data.movement': item.data.data.movement });
      // V10 changes
      actor.update({ 'system.movement': item.system.movement });
      // change End
    }
    if (item.type === 'knack' && item.parent instanceof Actor) {
      // let parentTalentName = item.data.data.sourceTalentName;
      // V10 changes
      let parentTalentName = item.system.sourceTalentName;
      // change End
      const actor = item.parent;
      const knackId = item.id;
      console.log(knackId);

      if (actor.items.getName(parentTalentName) === undefined) {
        ui.notifications.error("You Don't Have the Proper Talent");
        actor.deleteEmbeddedDocuments('Item', [knackId]);
        return;
      }
      let parentTalentID = actor.items.getName(parentTalentName).id;
      let parentTalent = actor.items.get(parentTalentID);
      // let parentTalentRanks = parentTalent.data.data.ranks
      // if (parentTalentRanks < item.data.data.talentRequirement){
      // V10 changes
      let parentTalentRanks = parentTalent.system.ranks;
      if (parentTalentRanks < item.system.talentRequirement) {
        // change End
        ui.notifications.error('Not Enough Ranks in Parent Talent');
        actor.deleteEmbeddedDocuments('Item', [knackId]);
      }
      // item.update({'data.sourceTalentId': parentTalentID});
      // V10 changes
      item.update({ 'system.sourceTalentId': parentTalentID });
      // change End
    }
    if (item.parent && item.img !== `icons/svg/item-bag.svg`) return;
    else if (item.img === `icons/svg/item-bag.svg`) {
      item.update({
        // change End
        img: `systems/earthdawn4e/assets/${item.type}.png`,
      });
      return item;
    }
  });

  Hooks.on('hotbarDrop', async (hotbar, dropData, slot) => {
    dropData = { ...dropData, ids: spliceUuid(dropData.uuid) };
    let actorId = '';
    let actor = {};
    if (dropData?.ids.actor) {
      actorId = dropData.ids.actor;
      actor = game.actors.get(actorId);
    } else if (dropData?.ids.token) {
      actorId = game.scenes.get(dropData.ids.scene).tokens.get(dropData.ids.token).actor.id;
      actor = game.scenes.get(dropData.ids.scene).tokens.get(dropData.ids.token).actor;
    }

    const item = actor.items.get(dropData.ids.item);
    if (item.type === 'attack') {
      ui.notifications.error('Hotbar does not yet support creature attacks');
      return;
    }
    if (item.type !== 'spellmatrix') {
      let functionName = 'rollPrep';

      const itemID = item.id;
      let itemType = '';
      let rollType = '';
      if (item.type === 'weapon') {
        itemType = 'weaponID';
        rollType = 'attack';
      } else {
        itemType = 'talentID';
      }

      const macroData = {
        name: item.name,
        command: `
                let character = game.actors.get('${actorId}');
                let inputs = {${itemType}: '${itemID}', 'rolltype': '${rollType}'};
                character.${functionName}(inputs)`,
        img: item.img,
      };

      let macro = await Macro.create({
        name: macroData.name,
        type: 'script',
        img: macroData.img,
        command: macroData.command,
      });
      await game.user.assignHotbarMacro(macro, slot);
    } else {
      const itemID = item.id;
      const itemName = item.name;
      const functionName = 'spellMatrixBox';
      const macroData = {
        name: item.name,
        command: `
                let character = game.actors.get('${actorId}');
                let inputs = {matrixID: '${itemID}', matrixName: '${itemName}'};
                character.${functionName}(inputs)`,
        img: item.img,
      };

      let macro = await Macro.create({
        name: macroData.name,
        type: 'script',
        img: macroData.img,
        command: macroData.command,
      });
      await game.user.assignHotbarMacro(macro, slot);
    }
  });

  Hooks.on('preCreateActor', (document, createData) => {
    // createData.type === 'creature' ? document.data.token.update({ actorLink: false }) : document.data.token.update({ actorLink: true });
    // V10 changes
    if (createData.isToken) {
      createData.type === 'creature' ? document.token.update({ actorLink: false }) : document.token.update({ actorLink: true });
    }
    // change End
  });

  async function assignSpellEffect(inputs, targetActorId) {
    console.log('Function is Running');
    let targetActor = game.actors.get(targetActorId)
    await targetActor.applyEffect(inputs);
  }

  async function assignDamage(inputs) {
    const targetToken = canvas.tokens.get(inputs.targetActor);
    const targetActor = targetToken.actor;
    await targetActor.takeDamage(inputs);
  }

  async function loadDisplay(introMessage, moduleName) {
    return await new Promise((resolve) => {
      let box_title = `Welcome to ${moduleName}`;
      new Dialog({
        title: box_title,
        content: introMessage,
        buttons: {
          ok: {
            label: game.i18n.localize('earthdawn.o.ok'),
            callback: (html) => {
              resolve({});
            },
          },
        },
        default: 'ok',
      }).render(true);
    });
  }

  async function importAdventures() {
    let packsArray = Array.from(game.packs);

    packsArray.forEach(async function (item, index) {
      let s = item.metadata.id;
      var fields = s.split('.');
      var id = fields[0];

      if (id !== 'world') {
        let moduleName = id;
        let modulesArray = Array.from(game.modules);

        let newModuleVersion = game.modules.get(moduleName).version;

        if (
          item.metadata.type === 'Adventure' &&
          newModuleVersion < game.settings.get('earthdawn4e', 'packsInitialized')[moduleName].moduleVersion
        ) {
          const adventureArray = Array.from(item.index);
          let workingModuleName = item.metadata.name;
          adventureArray.forEach(async function (item) {
            const adventureId = item._id;
            const adventureName = moduleName + '.' + workingModuleName;
            const adventurePack = game.packs.get(adventureName);
            const adventure = await adventurePack.getDocument(adventureId);

            console.log(adventure);
            await adventure.sheet._updateObject({}, new FormData());
          });

          let settingAdjust = game.settings.get('earthdawn4e', 'packsInitialized');
          settingAdjust[moduleName].loaded = true;
          settingAdjust[moduleName].systemVersion = game.system.version;
          settingAdjust[moduleName].moduleVersion = newModuleVersion;
          await game.settings.set('earthdawn4e', 'packsInitialized', settingAdjust);
        }
      }
    });
  }
});
